// [SECTION] Creating React Application:
	// Syntax:
		npx create-react-app <project-name>

// Delete unecesary files from the app
	// application > src
	App.test.js
	index.css
	logo.svg
	reportWebVitals.js

// Remove the importation of the "index.css" and "reportWebVitals" files from the "index.js" file. Also remove the code using the reportWebVitals function.
// Application > src > index.js

// Remove the importation of the "logo.svg" file and most of the codes found inside the "App" component to remove any errors.
// Application > src > App.js

// [SECTION] React JSX
/*
	The syntax used in Reactjs is JSX.

		- JSX - Javascript + XML, It is an extension of Javscript that let's us create objects which will then be compiled and added as HTML elements.

		- With JSX, we are able to create HTML elements using JS.

		- With JSX, we are able to create JS objects that will then be compiled and added as HTML elements.

*/

// [SECTION] ReactJS Component

/*
		- These are reusable parts of our react application.
		- They are independent UI parts of our app.
		- Components are functions that return react elements.
		- Components naming Convention: PascalCase
			- Capitalized letter for all words of the function name AND file name associated with it.
*/

/*
	Syntax:
		import { moduleName/s } from "file path"

*/

// [SECTION] React import pattern:

/*
		-imports from built-in react modules.
		-imports from downloaded packages
		-imports from user defined components
*/


// Documentations:
/*
	Virtual DOM
		https://www.youtube.com/watch?v=M-Aw4p0pWwg
	Built-in React Components
		https://react.dev/reference/react/components
	React-Bootstrap Documentation
		https://react-bootstrap.github.io/
*/