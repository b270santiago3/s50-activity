import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {

    console.log(courseProp);

    // Deconstructs the course properties into their own variables
    const { name, description, price } = courseProp;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

    // Syntax: const [getter, setter] = useState(initialGetterValue)
    const [ count, setCount ] = useState(0);

    // Use state hook for getting and setting the seats for this course
    const [seats, setSeats] = useState(30);

    // Function that keeps track of the enrollees for a course
    // By default JavaScript is synchronous, so it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
    // The setter function for useStates is asynchronous allowing it to execute separately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed resulting in the value to be displayed in the console to be behind by one count
    function enroll(){
        if (seats > 0) {
            setCount(count + 1);
            console.log('Enrollees: ' + count);
            setSeats(seats - 1);
            console.log('Seats: ' + seats);
        } else {
            alert("No more seats.");
        };  
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
