import { Fragment }  from 'react';
import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses() {
	// Checks to see if the mock data was captured
	console.log(coursesData);

	// map method loops through the individual course in our mock database and returns a CourseCard componnent for each course
	const courses = coursesData.map(course => {
		return (
				<CourseCard key={course.id} courseProp={course}/>
		)
	})

	return (
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>
	)
}