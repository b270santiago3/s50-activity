import { Form, Button } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register() {

    const { user } = useContext(UserContext);

    const [ email, setEmail ] = useState("");
    const [ password1, setPassword1 ] = useState("");
    const [ password2, setPassword2 ] = useState("");
    // Set to determine whether submit button is enabled or not
    const [ isActive, setIsActive ] = useState(false);


    console.log(email)
    console.log(password1)
    console.log(password2)

    useEffect(() => {
        if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
        setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password1, password2])

    function registerUser(e) {
        e.preventDefault();
        setEmail("");
        setPassword1("");
        setPassword2("");

        alert("Thank you for registering!")
    }


    return (
        (user.email !== null)
        ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email" 
                required
                value={email}
                onChange = {e => setEmail(e.target.value)}
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                required
                value={password1}
                onChange = {e => setPassword1(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label> Verify Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Verify Password" 
                required
                value={password2}
                onChange = {e => setPassword2(e.target.value)}
            />
          </Form.Group>

          {isActive 
            ?
            <Button variant="primary" type="submit"> Submit </Button>
            :
            <Button variant="danger" type="submit" disabled> Submit </Button> 
            }

        </Form>
    )
}
